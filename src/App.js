import React, { Component } from 'react';
import './App.css';
import Task from './Task';
import InputPart from './InputPart';

class App extends Component {
    state = {
        tasks: [],
        currentTask: ''
    };

    deleteTask = (id) => {
        const index = this.state.tasks.findIndex(p => p.id === id);
        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
        this.setState({tasks});
    };

    saveTaskMessage = (event) => {
        const state = {...this.state};
        state.currentTask = event.target.value;
        this.setState(state);
    };

    addTask = () => {
        const state = {...this.state};
        const task = state.currentTask;
        if(task.length !== 0){
            state.tasks.push({task, id: Date.now()});
            state.currentTask = "";
            this.setState(state);
        }
    }

    render() {
        return (
            <div className="App">
                <div className="container">
                    <InputPart
                        addTask={() => this.addTask()}
                        currentTask={(event) => this.saveTaskMessage(event)}
                        value={this.state.currentTask}/>
                    {this.state.tasks.map((task) => <Task
                        taskMessage={task.task}
                        key={task.id}
                        deleteTask={() => this.deleteTask(task.id)}

                    />)}
                </div>
            </div>
        );
    }
}

export default App;
