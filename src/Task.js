import React from 'react';

const Task = props => (
    <div className="task">
        <p className="taskMessage">{props.taskMessage}</p>
        <button className="deleteBtn" onClick={props.deleteTask}><i className="fa fa-trash" aria-hidden="true"></i></button>
    </div>
)

export default Task;